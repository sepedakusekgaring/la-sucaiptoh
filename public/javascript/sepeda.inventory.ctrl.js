/**
 * Created by raga on 11/10/2016.
 */
angular.module('sepedaGaring').controller('sepeda.inventory.ctrl', function($scope ,$http, $uibModal){

    $scope.array = [];

    function getData(){
        $http.get('/api/inventory/show', {}).success(function(reply){
            $scope.array = reply;
        });
    }

    $scope.format = 'dd-MMMM-yyyy';
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    function clear() {
        $scope.name = "";
        $scope.qty = "";
        $scope.price = "";
        $scope.unit = "";
    }
    
    $scope.BtnTambah = function(){
        var insert = {};
        insert.name    =  $scope.name;
        insert.qty     =  $scope.qty;
        insert.price   =  $scope.price;
        insert.unit    =  $scope.unit;
        $http.post("/api/inventory/insert", insert).success(function (reply){
            console.log(reply)
        });
        clear();
        getData();
    };

    $scope.onSearchChanged = function(){
        var params = {};
        params['search'] = $scope.search;
        $http.get('/api/inventory/show', {params: params}).success(function(reply){
           $scope.array = reply;
        });
    }

    $scope.onBtnModalClicked = function(){

        var modalInstance = $uibModal.open({
            templateUrl: '/modal/inventory',
            resolve: {

            }
        });

    };

    getData();


});