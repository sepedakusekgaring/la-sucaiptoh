
angular.module('sepedaGaring').controller('sepeda.home.ctrl', function($scope, $http){
    $scope.array = [];

    function clear(){
        $scope.nama   = "";
        $scope.alamat = "";
        $scope.asal   = "";
        $scope.umur   = "";
    }

    $scope.onBtnAddClicked = function () {
        var insert   = {};
        insert.name  = $scope.name;
        insert.qty   = $scope.qty;
        insert.price = $scope.price;
        insert.total = $scope.total;

        $scope.array.push(insert);

        $http.post('/api/daftar/insert', insert).success(function(reply){
            if(reply.error) return reply;
            return reply;
        });
        $scope.getData();
        clear();
    };


    $scope.getData = function(){
        $http.get('/api/daftar/show').success(function(reply){
            $scope.array = reply;
        });
    };

    $scope.getData();

    $scope.onBtnDeleteClicked = function ($index, row) {
        var dar= confirm("Are you sure want to delete this data?");

         if(dar){
            var params = {};
            params['id'] = row._id;
            $http.post('/api/daftar/remove', params).success(function(reply){
                console.log(reply);
            });
        } else {
            console.log(row._id, " deletion canceled!");
          }
        $scope.getData();
    };

    $scope.hasil = function (){
        $scope.total = $scope.qty * $scope.price;
    };

    $scope.onChooseUpdateClicked = function(lar,row){
        $scope.edit   = true;
        $scope.name   = row.name;
        $scope.qty    = row.qty;
        $scope.price  = row.price;
        $scope.total  = row.total;
        $scope.id     = row._id;
    };

    $scope.onBtnUpdateClicked = function(){
        $scope.edit   = false;
        var haha      = {};
        haha['name']  = $scope.name;
        haha['qty']   = $scope.qty;
        haha['price'] = $scope.price;
        haha['total'] = $scope.total;
        haha['id']    = $scope.id;

        $scope.array[$scope.Id] = haha;

        $scope.name  = "";
        $scope.qty   = "";
        $scope.price = "";
        $scope.total = "";

        $http.post('/api/daftar/update', haha).success(function(reply){
            console.log(reply);
        });

        clear();

        $scope.getData();



    };
});