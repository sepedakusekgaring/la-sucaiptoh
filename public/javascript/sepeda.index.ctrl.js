angular.module('sepedaGaring').controller('sepeda.index.ctrl', function($scope, $http){

  $scope.edit = false;

  $scope.array = [];

  function clear(){
    $scope.nama   = "";
    $scope.alamat = "";
    $scope.asal   = "";
    $scope.umur   = "";
  }

  $scope.onBtnAddClicked = function(){
    var insert = {};
    insert.nama   =  $scope.nama;
    insert.alamat =  $scope.alamat;
    insert.asal   =  $scope.asal;
    insert.umur   =  $scope.umur;
    $http.post('/api/biodata/insert', insert).success(function(reply){
      if(reply.error) return reply;
      return reply;
    });
    $scope.getData();
    clear();
  };

  $scope.getData = function(){
    $http.get('/api/biodata/show').success(function(reply){
      $scope.array = reply;
    });
  };

  $scope.getData();

  $scope.onBtnDeleteClicked = function($index, row){
    var c = confirm("Are you sure want to delete this data?");
    if(c){
      var params = {};
      params['id'] = row._id;
      $http.post('/api/biodata/remove', params).success(function(reply){
        console.log(reply);
      });
    } else {
      console.log(row._id, " deletion canceled!");
    }
    $scope.getData();
  };

  $scope.onChooseUpdateClicked = function(row){
    $scope.edit   = true;
    $scope.nama   = row.name;
    $scope.alamat = row.address;
    $scope.asal   = row.origin;
    $scope.umur   = row.age;
    $scope.Id = row._id;

  };

  $scope.onBtnUpdateClicked = function(row){
    $scope.edit       = false;
    var params        = {};
    params['nama']    = $scope.nama;
    params['umur']    = $scope.umur;
    params['alamat']  = $scope.alamat;
    params['asal']    = $scope.asal;
    params['id']      = $scope.Id;

    $http.post('/api/biodata/update', params).success(function(reply){
      console.log(reply);
    });

    clear();

    $scope.getData();
  };
});
