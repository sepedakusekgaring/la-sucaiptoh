var self                = {};
var mongojs             = require('mongojs');
var insert              = require('./insert/insert.js').insert;
var show              = require('./show/show.js').show;

self.insert = function(req,res){
    var name  = req.body.name  ? req.body.name.toUpperCase() :  null;
    var qty   = req.body.qty   ? parseFloat(req.body.qty) : 0;
    var price = req.body.price ? parseFloat(req.body.price) : 0;
    var unit  = req.body.unit  ? req.body.unit.toUpperCase() : null;
    insert(name, qty, price, unit, function(reply){
        return res.send(reply);
    });
};

self.show = function(req, res){
    var query = req.query.search ? new RegExp(req.query.search, "i") : null;

    show(query, function(reply){
        return res.send(reply);
    })
};

module.exports = self;