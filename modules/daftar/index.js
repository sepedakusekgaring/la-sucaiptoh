var self                = {};
var mongojs             = require('mongojs');
var insert              = require('./insert/insert.js').insert;
var update              = require('./update/update.js').update;
var show                = require('./show/show.js').show;
var remove              = require('./remove/remove.js').remove;

self.insert = function(req,res){
    var name      = req.body.name  ? req.body.name.toUpperCase() :  null;
    var qty       = req.body.qty   ? parseFloat(req.body.qty) : 0;
    var price     = req.body.price ? parseFloat(req.body.price) : 0;
    var total     = req.body.total ? parseFloat(req.body.total) : 0;

    insert(name, qty, price, total, function(reply){
        return res.send(reply);
    });
};

self.update = function(req, res){
    var id        = req.body.id    ? mongojs.ObjectId(req.body.id) : null;
    var name      = req.body.name  ? req.body.name.toUpperCase()   : null;
    var qty       = req.body.qty   ? parseFloat(req.body.qty)      : null;
    var price     = req.body.price ? parseFloat(req.body.price)    : null;
    var total     = req.body.total ? parseFloat(req.body.total)    : null;

    update(id, name, qty, price, total, function(reply){
        return res.send(reply);
    });
};

self.show = function(req,res){
    show(function(reply){
        return res.send(reply);
    });
};

self.remove = function(req, res){
    var id        = req.body.id ? mongojs.ObjectId(req.body.id) : null;

    remove(id, function(reply){
        return res.send(reply);
    });
};

module.exports = self;
