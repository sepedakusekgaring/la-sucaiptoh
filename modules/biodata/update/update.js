var mongojs             = require('mongojs');

exports.update = function(id, name, age, origin, address, callback){

    var update = {};
    update['name'] = name;
    update['age'] = age;
    update['origin'] = origin;
    update['address'] = address;

    db.collection('biodata').update(
        {_id: id},
        {$set: update},
        function(err, reply){
            if(err) return callback(err);
            return callback(reply);
        }
    );

};