var mongojs             = require('mongojs');

exports.insert = function(name, age, address, origin, callback){
    var insert       = {};
    insert['name'] = name;
    insert['age'] = age;
    insert['address'] = address;
    insert['origin'] = origin;
    db.collection('biodata').insert(insert, function(err, reply){
        if(err) return callback(err);
        return callback(reply);
    });
};
