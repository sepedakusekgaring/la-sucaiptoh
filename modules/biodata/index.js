var self                = {};
var mongojs             = require('mongojs');
var insert              = require('./insert/insert.js').insert;
var update              = require('./update/update.js').update;
var remove              = require('./remove/remove.js').remove;
var show                = require('./show/show.js').show;


self.insert = function(req,res){
  var name    = req.body.nama ? req.body.nama.toUpperCase() :  null;
  var age     = req.body.umur ? parseFloat(req.body.umur) : 0;
  var address = req.body.alamat ? req.body.alamat.toUpperCase() : null;
  var origin  = req.body.asal ? req.body.asal.toUpperCase() : null;
  insert(name, age, address, origin, function(reply){
    return res.send(reply);
  });
};

self.update = function(req, res){
  var id        = req.body.id ? mongojs.ObjectId(req.body.id) : null;
  var name      = req.body.name ? req.body.name.toUpperCase() : null;
  var origin    = req.body.origin ? req.body.origin.toUpperCase() : null;
  var address   = req.body.address ? req.body.address.toUpperCase() : null;
  var age       = req.body.age ? parseFloat(req.body.age) : null;

  update(id, name, age, origin, address, function(reply){
    return res.send(reply);
  });
};

self.remove = function(req, res){
  var id        = req.body.id ? mongojs.ObjectId(req.body.id) : null;

  remove(id, function(reply){
    return res.send(reply);
  });
};

self.show = function(req,res){
  console.log("asdsd")
  show(function(reply){
    return res.send(reply);
  });
};

module.exports          = self;
