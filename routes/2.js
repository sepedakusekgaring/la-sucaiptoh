var express     = require('express');
var router      = express.Router();
var mongojs     = require('mongojs');
var db          = mongojs('sucaiptoh');


/* GET index page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Express' });
});

router.post('/api/biodata', function(req, res, next){

  var bio       = {};
  bio['name']   = req.body.nama ? req.body.nama.toUpperCase() :  null;
  bio['age']    = req.body.umur ? parseFloat(req.body.umur) : 0;
  bio['address']= req.body.alamat ? req.body.alamat.toUpperCase() : null;
  bio['origin'] = req.body.asal ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').insert(bio, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.get('/api/biodata/show', function(req, res, next){

  db.collection('biodata').find({}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/delete', function(req, res, next){

  var id = req.body._id ? mongojs.ObjectId(req.body._id) : null;

  db.collection('biodata').remove({_id: id}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/update', function(req, res){

  console.log(req.body);

  var params        = {};
  params['name']    = req.body.nama   ? req.body.nama.toUpperCase() : null;
  params['age']     = req.body.umur   ? parseFloat(req.body.umur) : 0;
  params['address'] = req.body.alamat ? req.body.alamat.toUpperCase() : null;
  params['origin']  = req.body.asal   ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').update(
      {_id: mongojs.ObjectId(req.body.id)},
      {$set: params},
      function(err, reply){
        if(err) return res.send(err);
        return res.send(reply);
      }
  )

});

router.post('/api/biodata', function(req, res, next){

  var bio       = {};
  bio['name']   = req.body.nama ? req.body.nama.toUpperCase() :  null;
  bio['age']    = req.body.umur ? parseFloat(req.body.umur) : 0;
  bio['address']= req.body.alamat ? req.body.alamat.toUpperCase() : null;
  bio['origin'] = req.body.asal ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').insert(bio, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.get('/api/biodata/show', function(req, res, next){

  db.collection('biodata').find({}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/delete', function(req, res, next){

  var id = req.body._id ? mongojs.ObjectId(req.body._id) : null;

  db.collection('biodata').remove({_id: id}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/update', function(req, res){

  console.log(req.body);

  var params        = {};
  params['name']    = req.body.nama   ? req.body.nama.toUpperCase() : null;
  params['age']     = req.body.umur   ? parseFloat(req.body.umur) : 0;
  params['address'] = req.body.alamat ? req.body.alamat.toUpperCase() : null;
  params['origin']  = req.body.asal   ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').update(
      {_id: mongojs.ObjectId(req.body.id)},
      {$set: params},
      function(err, reply){
        if(err) return res.send(err);
        return res.send(reply);
      }
  )

});

  //Data base Home=====================================================================================================


router.post('/api/barang', function(req, res, next){

  var bar       = {};
  bar['name']   = req.body.nama  ? req.body.nama.toUpperCase() :  null;
  bar['qty']    = req.body.qty   ? parseFloat(req.body.qty) : 0;
  bar['price']  = req.body.price ? parseFloat(req.body.price) : 0;
  bar['total']  = req.body.total ? parseFloat(req.body.total) : 0;

  db.collection('barang').insert(bar, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.get('/api/barang/show', function(req, res, next){

  db.collection('barang').find({}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/barang/delete', function(req, res, next){

  var id = req.body._id ? mongojs.ObjectId(req.body._id) : null;

  db.collection('barang').remove({_id: id}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/barang/update', function(req, res){

  console.log(req.body);

  var params        = {};
  params['name']    = req.body.nama   ? req.body.nama.toUpperCase() : null;
  params['qty']     = req.body.qty    ? parseFloat(req.body.qty) : 0;
  params['price']   = req.body.price  ? parseFloat(req.body.price) : 0;
  params['total']   = req.body.total  ? parseFloat(req.body.total) : 0;

  db.collection('barang').update(
      {_id: mongojs.ObjectId(req.body.id)},
      {$set: params},
      function(err, reply){
        if(err) return res.send(err);
        return res.send(reply);
      }
  )

});

module.exports = router;
