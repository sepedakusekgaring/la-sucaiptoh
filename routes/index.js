var express     = require('express');
var router      = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Express' });
});


router.get('/inventory', function(req,res){
    res.render('inventory', {});
});

router.get('/modal/:name', function(req, res){
    var name = req.params.name;
    res.render('modal/' + name);
});

router.route('/api/:name/:method').all(function(req, res){
   var Modules  = require(__base + '/modules');
   var method   = req.params.method;
   var name     = req.params.name;
   if(Modules[name] && Modules[name][method]) {
       Modules[name][method](req, res);
   }
});

/**
router.post('/api/biodata', function(req, res, next){

  var bio       = {};
  bio['name']   = req.body.nama ? req.body.nama.toUpperCase() :  null;
  bio['age']    = req.body.umur ? parseFloat(req.body.umur) : 0;
  bio['address']= req.body.alamat ? req.body.alamat.toUpperCase() : null;
  bio['origin'] = req.body.asal ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').insert(bio, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.get('/api/biodata/show', function(req, res, next){

  db.collection('biodata').find({}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/delete', function(req, res, next){

  var id = req.body._id ? mongojs.ObjectId(req.body._id) : null;

  db.collection('biodata').remove({_id: id}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/biodata/update', function(req, res){

  console.log(req.body);

  var params        = {};
  params['name']    = req.body.nama   ? req.body.nama.toUpperCase() : null;
  params['age']     = req.body.umur   ? parseFloat(req.body.umur) : 0;
  params['address'] = req.body.alamat ? req.body.alamat.toUpperCase() : null;
  params['origin']  = req.body.asal   ? req.body.asal.toUpperCase() : null;

  db.collection('biodata').update(
      {_id: mongojs.ObjectId(req.body.id)},
      {$set: params},
      function(err, reply){
        if(err) return res.send(err);
        return res.send(reply);
      }
  )

});







router.post('/api/daftar', function(req, res, next){

  var bio       = {};
  bio['name']   = req.body.name  ? req.body.name.toUpperCase() : null;
  bio['qty']    = req.body.qty   ? parseFloat(req.body.qty)    : 0;
  bio['price']  = req.body.price ? parseFloat(req.body.price)  : 0;
  bio['total']  = req.body.total ? parseFloat(req.body.total)  : 0;

  db.collection('daftar').insert(bio, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.get('/api/daftar/show', function(req, res, next){

  db.collection('daftar').find({}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/daftar/delete', function(req, res, next){

  var id = req.body._id ? mongojs.ObjectId(req.body._id) : null;

  db.collection('daftar').remove({_id: id}, function(err, reply){
    if(err) return res.send(err);
    return res.send(reply);
  })

});

router.post('/api/daftar/update', function(req, res){

  console.log(req.body);

  var params        = {};
  params['name']    = req.body.name  ? req.body.name.toUpperCase() : null;
  params['qty']     = req.body.qty   ? parseFloat(req.body.qty) : 0;
  params['price']   = req.body.price ? parseFloat(req.body.price) : 0;
  params['total']   = req.body.total ? parseFloat(req.body.total): 0;

  db.collection('daftar').update(
      {_id: mongojs.ObjectId(req.body.id)},
      {$set: params},
      function(err, reply){
        if(err) return res.send(err);
        return res.send(reply);
      }
  )

});

*/
module.exports = router;
